package com.example.demo.mapper;

import com.example.demo.domain.BookCategory;
import com.example.demo.domain.BookCategoryBasic;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookCategoryMapper {

    @Select("select id, name from book_category")
    List<BookCategoryBasic> findAll();

    @Select("select * FROM book_category WHERE id = #{id}")
    @ResultMap("bookCategory")
    BookCategory findById(long id);

    @Delete("delete FROM book_category WHERE id = #{id}")
    int deleteById(long id);

    @Insert("insert INTO book_category(name) VALUES (#{name})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void insert(BookCategory bookCategory);

    @Update("Update book_category set name=#{name} where id=#{id}")
    void update(BookCategory bookCategory);
}
