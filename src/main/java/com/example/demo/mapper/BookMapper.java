package com.example.demo.mapper;

import com.example.demo.domain.Book;
import com.example.demo.domain.BookBasic;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookMapper {

    @Select("select\n" +
            "               b.id,\n" +
            "               b.name,\n" +
            "               bc.id as category_id,\n" +
            "               bc.name as category_name\n" +
            "        from book b\n" +
            "        left outer join book_category bc on b.book_category_id = bc.id")
    @ResultMap("book")
    List<Book> findAll();

    @Select("select\n" +
            "            b.id,\n" +
            "            b.name,\n" +
            "            bc.id as category_id,\n" +
            "            bc.name as category_name\n" +
            "        from book b\n" +
            "        left outer join book_category bc on b.book_category_id = bc.id\n" +
            "        where b.id = #{id}")
    @ResultMap("book")
    Book findById(long id);

    @Select("select id,name from book where book_category_id = #{id}")
    List<BookBasic> findAllByCategoryId(long id);

    @Delete("delete from book where id = #{id};")
    int deleteById(long id);

    @Insert("insert into book (name, book_category_id) values (#{name}, #{bookCategory.id})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void insert(Book book);

    @Update("update book set name = #{name}, set book_category_id = #{bookCategory.id} where id = #{id}")
    void update(Book book);

}
