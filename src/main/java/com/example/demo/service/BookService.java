package com.example.demo.service;

import com.example.demo.domain.Book;
import com.example.demo.domain.BookBasic;
import com.example.demo.mapper.BookMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class BookService {
    private final BookMapper bookMapper;

    public List<Book> findAll() {
        return bookMapper.findAll();
    }

    public Book findById(Long id) {
        return bookMapper.findById(id);
    }

    public List<BookBasic> findAllByCategoryId(Long id) {
        return bookMapper.findAllByCategoryId(id);
    }

    public long insert(Book book) {
         bookMapper.insert(book);
         return book.getId();
    }

    public void update(Book book) {
        bookMapper.update(book);
    }

    public int deleteById(Long id) {
        return bookMapper.deleteById(id);
    }

}
