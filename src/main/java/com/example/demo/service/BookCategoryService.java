package com.example.demo.service;

import com.example.demo.domain.BookCategory;
import com.example.demo.domain.BookCategoryBasic;
import com.example.demo.mapper.BookCategoryMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class BookCategoryService {
    private final BookCategoryMapper bookCategoryMapper;

    public List<BookCategoryBasic> findAll() {
        return bookCategoryMapper.findAll();
    }

    public BookCategory findById(Long id) {
        return bookCategoryMapper.findById(id);
    }

    public long insert(BookCategory bookCategory) {
        bookCategoryMapper.insert(bookCategory);

        return bookCategory.getId();
    }

    public void update(BookCategory bookCategory) {
        bookCategoryMapper.update(bookCategory);
    }

    public int deleteById(Long id) {
        return bookCategoryMapper.deleteById(id);
    }

}
