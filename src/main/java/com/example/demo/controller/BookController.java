package com.example.demo.controller;

import com.example.demo.domain.Book;
import com.example.demo.domain.BookBasic;
import com.example.demo.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/book")
public class BookController {

    private final BookService bookService;

    @GetMapping()
    public List<Book> findAll() {
        return bookService.findAll();
    }

    @GetMapping("/{id}")
    public Book findById(@PathVariable Long id) {
        return bookService.findById(id);
    }

    @GetMapping("/by-category/{id}")
    public List<BookBasic> findAllByCategoryId(@PathVariable Long id) {
        return bookService.findAllByCategoryId(id);
    }

    @PostMapping()
    public long save(@RequestBody Book book) {
        return bookService.insert(book);
    }

    @PutMapping()
    public void update(@RequestBody Book book) {
        bookService.update(book);
    }

    @DeleteMapping("/{id}")
    public long delete(@PathVariable Long id) {
        return bookService.deleteById(id);
    }



}
