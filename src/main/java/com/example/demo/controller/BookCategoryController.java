package com.example.demo.controller;

import com.example.demo.domain.BookCategory;
import com.example.demo.domain.BookCategoryBasic;
import com.example.demo.service.BookCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/book-category")
public class BookCategoryController {

    private final BookCategoryService bookCategoryService;

    @GetMapping()
    public List<BookCategoryBasic> findAll() {
        return bookCategoryService.findAll();
    }

    @GetMapping("/{id}")
    public BookCategory findById(@PathVariable Long id) {
        return bookCategoryService.findById(id);
    }

    @PostMapping()
    public long save(@RequestBody BookCategory bookCategory) {
        return bookCategoryService.insert(bookCategory);
    }

    @PutMapping()
    public void update(@RequestBody BookCategory bookCategory) {
        bookCategoryService.update(bookCategory);
    }

    @DeleteMapping("/{id}")
    public long delete(@PathVariable Long id) {
        return bookCategoryService.deleteById(id);
    }

}
