create sequence seq_book_category
    cache 10;

CREATE TABLE "book_category" (
    "id" bigint DEFAULT nextval('seq_book_category') PRIMARY KEY NOT NULL,
    "name" varchar(255) NOT NULL
);


