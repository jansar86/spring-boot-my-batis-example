create sequence seq_book
    cache 10;

CREATE TABLE "book" (
    "id" bigint DEFAULT nextval('seq_book') PRIMARY KEY NOT NULL ,
    "name" varchar(255) NOT NULL,
    "book_category_id" bigint NOT NULL,
    CONSTRAINT fk_book_book_category FOREIGN KEY(book_category_id) REFERENCES book_category(id)
);
